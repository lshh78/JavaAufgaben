Dieses Übungsblatt soll Ihnen helfen, sich mit dem Stoff aus der Vorlesung und der Übung eigenständig und aktiv auseinanderzusetzen. Vielleicht mögen Sie die Aufgaben in einer Lerngruppe bearbeiten. Gemeinsames Lernen hilft enorm, wenn Sie einmal stecken bleiben und nicht weiter wissen. Und Sie können sich gegenseitig Dinge erklären.

Der Sinn und Zweck einer Übungsaufgabe liegt darin, dass Sie sich selber daran versuchen. Auch wenn Sie Zugriff auf eine Lösung haben, versuchen Sie es erst für sich oder ziehen Sie Ihre Aufzeichnungen und Unterlagen zu Rate. Auch ein Gespräch oder eine Diskussion mit Ihren Studienkolleg(inn)en ist wertvoller als ein voreiliger Blick in eine Lösung.

Lernen ist ein bißchen anstrengend. Aber es macht auch Spaß, wenn man Erfolge sieht und eine Aufgabe geknackt hat. Wie Sie wissen: Übung macht den Meister!

// http://shapecatcher.com/unicode/block/Dingbats