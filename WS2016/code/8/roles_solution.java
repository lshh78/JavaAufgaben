enum AccessLevel {
  GUEST, DEVELOPER, ADMIN;
}

class UserInt {
  static final int ACCESS_GUEST = 0;
  static final int ACCESS_DEVELOPER = 1;
  static final int ACCESS_ADMIN = 2;
  String name;
  int access;
  UserInt(String name, int access) {
    this.name = name;
    this.access = access;
  }
  boolean canUploadFiles() {
    switch(access) {
      case ACCESS_GUEST:
        return false;
      default:
        return true;
    }
  }
}

class UserEnum {
  String name;
  AccessLevel access;
  UserEnum(String name, AccessLevel access) {
    this.name = name;
    this.access = access;
  }
  boolean canUploadFiles() {
    switch(access) {
      case GUEST:
        return true;
      default:
        return false;
    }
  }
}
