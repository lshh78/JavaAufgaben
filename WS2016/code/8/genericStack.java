class Stack<E> {
  E value;
  Stack<E> rest;
  Stack(E value, Stack<E> rest) {
    this.value = value;
    this.rest = rest;
  }
  Stack(E value) {
    this(value, null);
  }
  public String toString() {
    if(rest == null) {
      return value.toString();
    } else {
      return value + "\n" + rest.toString();
    }
  }
  int size() {
    if(rest == null) return 1;
    return 1 + rest.size();
  }
  void push(E val) {
    rest = new Stack<E>(value, rest);
    value = val;
  }
  E pop() {
    E top = value;
    value = rest.value;
    rest = rest.rest;
    return top;
  }
  E[] toArray(E[] base) {                   // <1>
    E[] ar = Arrays.copyOf(base, size());
    int i = 0;
    Stack<E> cur;
    for(cur = this; cur.rest != null ; cur = cur.rest) {
      ar[i++] = cur.value;
    }
    ar[i] = cur.value;
    return ar;
  }
  static <E> Stack fromArray(E[] ar) {      // <2>
    Stack<E> s = null;
    for(int i = 0; i < ar.length; i++) {
      s = new Stack<E>(ar[i], s);
    }
    return s;
  }
}
