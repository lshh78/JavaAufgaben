import java.io.*;
import java.util.regex.Pattern;

class Grep {
    public static void main(String[] args) {
        int counter = 0;
        BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
        String line;
        Pattern p = Pattern.compile(args.length > 0 ? args[0] : ".*",Pattern.MULTILINE);
        try {
            while((line = in.readLine()) != null) { 
                if (p.matcher(line).matches()) {
                    if (args.length == 2 && args[1].equals("-c")) 
                        counter++;
                    else
                        System.out.println(line);
                }
            }
        } catch (IOException e) {
            System.out.println("No input available!");
            System.exit(1);
        }
        if (args.length == 2 && args[1].equals("-c")) 
            System.out.println(counter);
        System.exit(0);
    }
}

