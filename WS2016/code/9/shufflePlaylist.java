import java.util.List;
import java.util.ArrayList;
import java.nio.file.Path;
import java.nio.file.Paths;

<E> void shuffle(List<E> lst) {              // <1>
  Random r = new Random();
  for(int i = 0; i < lst.size(); i++) {
    int j = r.nextInt(lst.size());
    E tmp = lst.get(j);
    lst.set(j, lst.get(i));
    lst.set(i, tmp);
  }
}

List<String> readPlaylist(String inName) throws IOException {
  Charset utf8 = StandardCharsets.UTF_8;
  List<String> lst = new ArrayList<>();
  try(BufferedReader br = Files.newBufferedReader(Paths.get(inName), utf8)) {
    String line;
    while((line = br.readLine()) != null) {
      lst.add(line);
    }
  }
  return lst;
}

void writePlaylist(List<String> playlist, String outName) throws IOException {
  Charset utf8 = StandardCharsets.UTF_8;
  try(BufferedWriter br = Files.newBufferedWriter(Paths.get(outName), utf8)) {
    for(String fname : playlist) {
      br.write(fname);
      br.write("\n");
    }
  }
}

List<String> playlist = readPlaylist("D:\\temp\\test.m3u");
shuffle(playlist);
printf(playlist.toString());
writePlaylist(playlist, "D:\\temp\\test.m3u");
