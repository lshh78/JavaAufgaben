import java.math.BigInteger;

class Fact {
    
    public static void main(String[] args) {
        BigInteger n;
        try {
            n = new BigInteger(args[0]);
            n = n.abs();
            System.out.println(factorial(n));
        } catch (NumberFormatException | ArrayIndexOutOfBoundsException e) {
            System.out.println("Supply an integer as an argument!");
            System.exit(1);
        }
        System.exit(0);
    }

    static BigInteger factorial(BigInteger n) {
        if (n.equals(BigInteger.ZERO)) return BigInteger.ONE;
        return n.multiply(factorial(n.subtract(BigInteger.ONE)));
    }
}


