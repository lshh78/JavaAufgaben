class Point implements Comparable<Point> {
  int x;
  int y;
  Point(int x, int y) {
    this.x = x;
    this.y = y;
  }
  public int compareTo(Point other) {      // <1>
    if(x == other.x) return y - other.y;
    return x - other.x;
  }
  public String toString() {               // <2>
    return "("+x+", "+y+")";
  }
}

Point[] points = {
  new Point(6,3),
  new Point(3,7),
  new Point(3,3)
};
Arrays.sort(points);
printf(Arrays.toString(points));