class Video {
  String title;
  int dur;
  boolean subtitles = false;
  Video(String title, int dur) {
    this.title = title;
    this.dur = dur;
  }
  void play() {
    printf("Playing %s\n", title);
  }
  void setSubtitles(boolean on) {
    subtitles = on;
  }
  int getDurationSeconds() {
    return dur;
  }
}

class Album {
  String title;
  String[] tracks;
  int[] trackDurations;
  int currentTrack = 0;
  Album(String title, String[] tracks, int[] trackDurations) {
    this.title = title;
    this.tracks = tracks;
    this.trackDurations = trackDurations;
  }
  void play() {
    printf("Playing %s\n", title);
    printf("Current track: %s\n", tracks[currentTrack]);
  }
  void nextTrack() {
    currentTrack = (currentTrack + 1) % tracks.length;
  }
  int getDurationSeconds() {
    int sum = 0;
    for(int dur: trackDurations) { sum += dur; }
    return sum;
  }
}