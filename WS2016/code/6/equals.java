class Point {
  int x;
  int y;
  Point(int x, int y) {
    this.x = x;
    this.y = y;
  }
  public equals(Object other) {
    if (other instanceof Point) {          // <1>
      Point otherPoint = (Point) other;    // <2>
      return otherPoint.x == this.x && otherPoint.y == this.y;
    }
    return false;
  }
}

class Car {
  double tachometer;
  Car(double tachometer) {
    this.tachometer = tachometer;
  }
  public equals(Object other) {
    if (other instanceof Car) {           // <1>
      Car otherCar = (Car) other;         // <2>
      return otherCar.tachometer == this.tachometer;
    }
  }
}


class Fraction {
  private final long numerator;
  private final long denominator;
  Fraction(long numerator) {
    this(numerator, 1L);
  }
  Fraction(long numerator, long denominator) {
    this.numerator = numerator;
    this.denominator = denominator;
  }
  public equals(Object other) {
    if (other instanceof Fraction) {          // <1>
      Fraction otherFrac = (Fraction) other;  // <2>
      return otherFrac.numerator == this.numerator
          && otherFrac.denominator == this.denominator;
    }
    return false;
  }
}