boolean test1() {
    Quizz q = new Quizz(10); // <= 5?
    q.answer(true); // <= 3?
    q.answer(true); // <= 2?
    q.answer(true); // <= 1?
    return q.answer(true).equals("The number is 1");
}

boolean test2() {
    Quizz q = new Quizz(10); // <= 5
    q.answer(true); // <= 3;
    q.answer(true); // <= 2;
    q.answer(true); // <= 1;
    return q.answer(false).equals("The number is 2");
}

boolean test3() {
    Quizz q = new Quizz(10); // <= 5
    q.answer(true); // <= 3?
    q.answer(true); // <= 2?
    return q.answer(false).equals("The number is 3");
}

boolean test4() {
    Quizz q = new Quizz(10); // <= 5?
    q.answer(true); // <= 3?
    q.answer(false); // <= 4
    return q.answer(true).equals("The number is 4");
}

boolean test5() {
    Quizz q = new Quizz(10); // <= 5?
    q.answer(true); // <= 3?
    q.answer(false); // <= 4?
    return q.answer(false).equals("The number is 5");
}

Boolean[] test = {
    test1(), test2(), test3(), test4(), test5()
}

printf("%d tests: discovered %d error(s)!\n",
    test.length,
    Arrays.stream(test).filter(n -> n == false).count());