int sum4(int[] a) {
    int sum = 0;
    for (int i = 0; i < 4 * a.length; i++)
        sum += a[i % a.length];
    return sum;
}

int[] test = {1,2,3,4};
if (sum4(test) != 4 * Arrays.stream(test).sum())
    throw new AssertionError();
