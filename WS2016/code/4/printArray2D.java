void printArray2D(int[][] a) {
    for(int i = 0; i < a.length; i++) {
        for(int j = 0; j < a[i].length; j++) {
            printf("%2d ", a[i][j]);
        }
        printf("\n");
    }
}

int[][] test = {{-1,2,0},{3,-5,6},{-1,0,-2}};
