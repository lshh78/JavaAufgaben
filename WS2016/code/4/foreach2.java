int max(int[] ar) {
  int max = Integer.MIN_VALUE;
  for(int x : ar) {
    if(x > max) max = x;
  }
  return max;
}