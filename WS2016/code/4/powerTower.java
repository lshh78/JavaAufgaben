double powerTower(double x, int n) {
  if(n == 0) return 1;
  return Math.pow(x, powerTower(x, n-1));
}