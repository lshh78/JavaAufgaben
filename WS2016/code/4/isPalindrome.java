boolean isPalindrome(String s) {
  if (s.length() < 2) return true;
  int last = s.length()-1;
  return s.charAt(0) == s.charAt(last) && isPalindrome(s.substring(1,last));
}