int rollDoubles() {
  Random r = new Random();
  int x = 0;
  int y = -1;                     // <1>
  int i = 0;                      
  for(; x != y; i++) {            // <2>
    x = r.nextInt(6) + 1;
    y = r.nextInt(6) + 1;
  }
  return i;
}