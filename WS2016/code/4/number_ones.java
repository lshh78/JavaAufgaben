/* Diese Implementierung nutzt unnötigerweise einen
   String s als Zwischenspeicher und else if.
*/
String ones_elseif(int n) { // Variante mit else if
    // n ist Zahl von 0 bis 9
    String s = "";
    if (n == 0) s = "null";
    else if (n == 1) s = "eins";
    else if (n == 2) s = "zwei";
    else if (n == 3) s = "drei";
    else if (n == 4) s = "vier";
    else if (n == 5) s = "fünf";
    else if (n == 6) s = "sechs";
    else if (n == 7) s = "sieben";
    else if (n == 8) s = "acht";
    else s = "neun";
    return s;   
}

/* Ein return beendet den Methodenaufruf.
   Deshalb ist kein else if nötig.
*/
String ones_if(int n) { // Variante mit if
    if (n == 0) return "null";
    if (n == 1) return "eins";
    if (n == 2) return "zwei";
    if (n == 3) return "drei";
    if (n == 4) return "vier";
    if (n == 5) return "fünf";
    if (n == 6) return "sechs";
    if (n == 7) return "sieben";
    if (n == 8) return "acht";
    return "neun";    
}

/* Die switch-Anweisung ist eine Alternative.
   Wegen der return-Anweisungen ist das
   übliche break überflüssig.
*/
String ones_switch(int n) { // Variante mit switch
    switch (n) {
        case 0: return "null";
        case 1: return "eins";
        case 2: return "zwei";
        case 3: return "drei";
        case 4: return "vier";
        case 5: return "fünf";
        case 6: return "sechs";
        case 7: return "sieben";
        case 8: return "acht";
        case 9: return "neun";
        default: return "ähh";
    }
}

/* Diese Umsetzung ist schlank und datengetrieben.
   Der Modulooperator sorgt für stets gültige Ergebnisse,
   sofern n >= 0 ist.
*/
String ones(int n) { // finale Implementierung
    String[] ones = {"null", "eins", "zwei", "drei", "vier",
                     "fünf", "sechs", "sieben", "acht", "neun"};
    return ones[n % 10];
}

void testOnes() { // Check, ob alle Methoden gleichwertig sind
    for(int i = 0; i <= 9; i++) {
        if (ones(i) != ones_elseif(i)) throw new AssertionError();
        if (ones(i) != ones_if(i)) throw new AssertionError();
        if (ones(i) != ones_switch(i)) throw new AssertionError();
    }
}
