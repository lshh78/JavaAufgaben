class Point {
  int x;
  int y;
  Point(int x, int y) {
    this.x = x;
    this.y = y;
  }
  float dist(Point other) {
    return Math.sqrt(Math.pow(this.x - other.x, 2) 
                   + Math.pow(this.y - other.y,2));
  }
  PolarPoint toPolar() {
    double alpha = Math.atan2(y,x);
    double dist =  dist(new Point(0,0));
    return new PolarPoint(alpha, dist);
  }
}

class PolarPoint {
  double alpha;     // <1>
  double dist;
  Point(double alpha, double dist) {
    this.alpha = alpha;
    this.dist = dist;
  }
  Point toCartesian() {
    int x = Math.round(Math.cos(alpha) * dist);
    int y = Math.round(Math.sin(alpha) * dist);
    return new Point(x, y);
  }
}