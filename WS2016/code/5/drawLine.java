char[][] image = new char[10][20];

void printImage(char[][] image) {
  for(int y = 0; y < image.length; y++) {
    for(int x = 0; x < image[y].length; x++) {
      printf(String.valueOf(image[y][x]));
    }
    printf("\n");
  }
}

void drawLine(char[][] image, Line line) {
  // Bresenham's line drawing algorithm
  double dx = line.p2.x - line.p1.x;
  double dy = line.p2.y - line.p1.y;
  int xstep = dx > 0 ? 1 : -1;
  int ystep = dy > 0 ? 1 : -1;
  double error = -1.0;
  double deltaerr = Math.abs(dy / dx);
  int y = line.p1.y;
  for(int x = line.p1.x; x * xstep <= line.p2.x * xstep; x+= xstep) {
    image[y][x] = '#';
    error += deltaerr;
    if (error >= 0) {
      y += ystep;
      error -= 1;
    }
  }
}

Line l = new Line(0,0,10,5);
drawLine(image, l);
printImage(image);