jshell> Car car = new Car();
car ==> Car@711f39f9

jshell> car.setZero()

jshell> car.drive(120,30)

jshell> car.tachometer
$9 ==> 60.0

jshell> Car.mileage(4.0,car.tachometer)
$10 ==> 6.666666666666667