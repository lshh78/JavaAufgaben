# Java-Aufgaben

Eine Sammlung von Aufgaben zur Java-Programmierung findet sich in diesem Repo. Die Aufgaben sind entstanden im Rahmen der Veranstaltung "Objektorientierte Programmierung".

Um Programme für Java entwickeln zu können, müssen Sie das _Java Development Kit_ (JDK) auf Ihrem Rechner installieren:

* [Installationsanleitung](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/0.InstallationJDK.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/0.InstallationJDK.adoc))

Für das WS2017 sind zur Bearbeitung und Ansicht fertiggestellt die folgenden Pools an Aufgaben:

> ACHTUNG: Die Texte nutzen ein relativ neues HTML-Feature, das Tag `<details>`, damit Sie Lösungen wahlweise auf- und zuklappen können. Leider funktioniert das Tag (noch) nicht in Microsofts Internet Explorer. Verwenden Sie deshalb einen anderen, aktuellen Browser.

* [Formale Sprachen und Grammatiken](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/1.FormaleGrammatik.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/1.FormaleGrammatik.adoc))
* [Grundlagen und Arrays](http://htmlpreview.github.io/?https://git.thm.de/dhzb87/JavaAufgaben/raw/master/2.GrundlagenUndArrays.html) ([Original](https://git.thm.de/dhzb87/JavaAufgaben/blob/master/2.GrundlagenUndArrays.adoc))


### Logo-Quelle

Das [Logo](https://pixabay.com/de/caf%C3%A9-java-logo-kaffee-151346/) stammt aus Pixabay und ist frei nutzbar.