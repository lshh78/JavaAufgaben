= 3. Woche: Fragen und Aufgaben zu OOP
Dominikus Herzberg, Nadja Krümmel, Christopher Schölzel
2016-10-28, 16 Uhr
:toc:
:icons: font
:solution:
:stem:

CAUTION: In einigen Aufgaben geht es um Schleifen. Versuchen Sie, auch Lösungen statt mit der `for`-Schleife mit einem `while` oder einem `do/while` zu erarbeiten.

== Zeichenketten (_Strings_)

=== Kurz-Einführung zu Zeichenketten

Für Zeichenketten (_strings_) gibt es in Java eine eigene Klasse namens `String`. Da Zeichenketten so wichtig in der Datenverarbeitung sind, gibt es in praktisch jeder Programmiersprache das String-Literal: Die Zeichenkette wird eingerahmt in doppelte Hochkommata.

    jshell> String s = "Ich studiere an der THM"
    s ==> "Ich studiere an der THM"

Mit dem Plus-Operator lassen sich nicht nur Zahlen addieren, sondern auch Zeichenketten verbinden (konkatenieren).

    jshell> "Hello " + "World" + "!"
    $34 ==> "Hello World!"

Möchte man eine Zeichenkette auf der Konsole ausgeben, hilft einem die Methode `println` des Objekts `out` aus der Klasse `System` weiter. Beachten Sie das ergänzte Ausrufezeichen am Ende der Ausgabe.

    jshell> System.out.println(s + "!")
    Ich studiere an der THM!

=== Recherche: Was die Klasse String so kann!

Finden Sie über die JShell heraus, welche Methoden die Klasse `String` abietet. Wählen Sie die alphabetisch letzte Methode aus und finden Sie heraus (Internet-Recherche), was die Methode macht. Geben Sie zwei Beispiele an.

ifdef::solution[]
.Lösung
Mit der Tabulator-Taste liefert die JShell nach der Eingabe des Punktes folgendes zurück:
----
jshell> String.
CASE_INSENSITIVE_ORDER   class      copyValueOf(
format(                  join(      valueOf(
----
Gemeint is `valueOf`, das für eine Reihe von Typen die Repräsentation eines übergebenen Werts als Zeichenkette zurückgibt. Beispiele sind:
----
jshell> String.valueOf(13.5)
$43 ==> "13.5"

jshell> String.valueOf(0b1010)
$50 ==> "10"
----
endif::solution[]

=== Recherche: Was ein String-Objekt so kann!

In der vorigen Aufgabe haben Sie untersucht, welche Methoden Ihnen die Klasse `String` anbietet. Erzeugen Sie ein String-Objekt mithilfe des String-Literals:
----
String s = "Hi";
----
Finden Sie heraus, welche Methoden das String-Objekt `s` anbietet. Suchen Sie die offizielle Dokumentation zur String-Klasse von der Firma Oracle heraus. Lesen Sie dort nach: Was macht `length()`, was `split()`? Wenden Sie die Methoden an einem kleinen Beispiel an.

ifdef::solution[]
.Lösung
In der JShell `s.` samt Tabulatortaste drücken, und eine Liste der verfügbaren Methoden erscheint.

Die Doku: https://docs.oracle.com/javase/8/docs/api/java/lang/String.html. Es ist die Fassung für Java Standard Edition (Java SE), Version 8, relevant. (Noch gibt es keine offizielle Dokumentation zu Java 9.)

    s.length()
    "Guten Tag!".length()
    "Kakadu".split("ka")
    "Kakadu".split("ka")[0]
endif::solution[]

=== Alter in Filmen

Es gibt Filme, die sind zugelassen für ein Alter ab 0 Jahren ("Jeden"), 6 Jahren ("Schulkinder"), 12 ("Jugendliche"), 16 ("reife Jugendliche") und 18 ("Erwachsene") Jahren. Sie sollen ein Programm schreiben, das auf eine Altersangabe hin eine Zeichenkette zurückgibt, die im Klartext angibt, welche Zuschauergruppe den Film sehen darf.

==== Ohne Methode

Gehen Sie von folgendem Code-Vorspann aus:

----
int age = 0; // Die Zahl dürfen Sie ändern
String group = "";
----

Schreiben Sie den Code, der diesen Zeilen folgt, der die Variable `group` abhängig von `age` verändert. Für einen Wert `age` von z.B. `3` (gemeint sind "3 Jahre") wird `group` auf `"Jeden"` gesetzt; für `12` oder z.B. `13` auf
`"Jugendliche"` usw.

* Schreiben Sie Ihre Entscheidungslogik mit Hilfe einer `if`-Anweisungen.
* In einer anderen Variante verwendenn Sie bitte die `switch`-Anweisung.

==== Mit Methode

Packen Sie Ihre Ausführungslogik in den Rumpf einer Methode namens `allowedFor`. Die Methode nimmt das Alter als Integer entgegen und liefert einen String als Ergebnis zurück.

----
String allowedFor(int age) {
    // Your Solution
}
----

Mit einem gegebenen Alter soll die Zeichenkette angeben, für wen der Film alles geeignet ist. Als Beispiel folgende Interaktion mit der JShell:

----
jshell> allowedFor(15)
$32 ==> "Jeden, Schulkinder, Jugendliche"

jshell> allowedFor(3)
$33 ==> "Jeden"
----

* Setzen Sie den Rumpf um mit Hilfe von `if`-Anweisungen.
* Setzen Sie den Rumpf alternativ um mit einer `switch`-Anweisung.

=== Zählen in Strings

Schreiben Sie ein kleines Programmfragment, das die Vorkommen von dem String `needle` in dem String `haystack` zählt. (Tipp: Nutzen Sie das zuvor recherchierte Wissen zu Strings! Die Lösung ist ein Einzeiler. ;-) )

  String haystack = "Donaudampfschifffahrtsgesellschaftskapitänsmützenbommel";
  String needle = "ts";

ifdef::solution[]
.Lösung
----
haystack.split(needle).length - 1
----
endif::solution[]

=== Stringdekonstruktion

Zerlegen Sie die folgende Zeichenkette mit Java-Befehlen in ihre Bestandteile und geben Sie die Differenz zwischen der ersten und letzten Zahl an. Funktioniert Ihr Programm immer noch, wenn Sie die Anzahl der Zahlen in der Zeichenkette ändern?

TIP: Die Aufgabe ist anspruchsvoll. Sie werden die außerdem die Methode `substring` und die Methode `Integer.parseInt` benötigen. Lesen Sie zuvor die Dokumentation dazu.

  String stuff = "[1,2,4,7,22,91]";

ifdef::solution[]
.Lösung
----
String[] numbers = stuff.substring(1, stuff.length()-1).split(",")
int first = Integer.parseInt(numbers[0]);
int last = Integer.parseInt(numbers[numbers.length-1]);
printf("Differenz: %d\n",last - first);
----
endif::solution[]

=== Drei Chinesen

Schreiben Sie ein Programmfragment, das ähnlich wie bei dem Kinderlied "Drei Chinesen mit dem Kontrabass" alle Vokale im String `stanza` durch den Vokal ersetzt, der in der Variable `vowel` gespeichert ist.

  String stanza = "Drei Chinesen mit dem Kontrabass ...";
  char vowel = 'o';

ifdef::solution[]
.Lösung
----
stanza = stanza.replace("a",vowel);
stanza = stanza.replace("e",vowel);
stanza = stanza.replace("i",vowel);
stanza = stanza.replace("o",vowel);
stanza = stanza.replace("u",vowel);
----
endif::solution[]

== Textformatierung

Ausgaben auf der Konsole macht man mit einer der drei print-Methoden von `System.out`: `print`, `printf` oder `println`. Eine einfache typische print-Ausgabe sieht wie folgt aus:

    jshell> System.out.println("7 + 3 = " + (7 + 3));
    7 + 3 = 10

Interessanterweise steht einem die Methode `printf` in der JShell direkt zur Verfügung. Man kann dieser Methode eine Zeichenkette mitgeben, die Informationen zur Formattierung der auszugebenden Zeichenkette enthält:

    jshell> printf("7 + 3 = %d", 7 + 3)
    7 + 3 = 10

Ein einfaches "Hallo Welt!" mit anschließendem Zeilenumbruch sieht in der JShell so aus:

    jshell> printf("Hallo Welt!\n")
    Hallo Welt!

=== Zahlen formatieren

Geben Sie die im folgenden Code definiterten Zufallszahlen mit Hilfe der Methode `printf`

. linksbündig
. rechtsbündig

aus.

    Random r = new Random();
    int var1 = r.nextInt(1000);
    int var2 = r.nextInt(1000);

Beispiele für eine linksbündige Ausgabe:

    754
    17

Beispiele für eine rechtsbündige Ausgabe:

    754
     17

Funktioniert Ihre Lösung auch noch, wenn Sie hinter jeder Zahl das Zeichen `|` ausgeben?

ifdef::solution[]
.Lösung
----
// rechtsbündig
printf("%3d|\n", var1);  <1>
printf("%3d|\n", var2);
// linksbündig
printf("%-3d|\n", var1); <2>
printf("%-3d|\n", var2);
----
<1> Das Zeilenumbruchszeichen `\n` ist optional, da bei der Ausgabe auf der JShell sowieso ein Zeilenumbruch hinzugefügt wird. Spätestens bei längeren Ausgaben in einer Schleife werden Sie es aber brauchen.
<2> Eine einfachere Lösung ist `printf("%d", var1)`, aber dann werden keine Leerzeichen am Ende angehängt.
endif::solution[]

== Verzweigungen

=== PQ-Formel
Schreiben Sie ein Programmfragment, dass für die unten angegebenen Variablen `p` und `q` das Ergebnis der sogenannten https://de.wikipedia.org/wiki/Quadratische_Gleichung#p-q-Formel[p-q-Formel] berechnet. Unterscheiden Sie per `if`-Anweisung (samt `else if`), ob es eine, zwei oder keine Lösung gibt. Der Nutzer soll das Ergebnis bzw. die Ergebnisse per `printf` ausgegeben bekommen. Dazu soll der Hinweis ausgegeben werden, ob es keine, eine oder zwei Lösungen gibt.

Speichern Sie das Fragment in einer Datei und laden Sie es mit dem Befehl `/open` in der JShell nach den folgenden Codezeilen, um die Berechnung durchführen zu lassen.

  Random r = new Random();
  int p = r.nextInt();
  int q = r.nextInt();

ifdef::solution[]
.Lösung
Wenn der Term unter der Wurzel der p-q-Formel (nachfolgend `rad` genannt) Null ist, dann gibt es nur eine Lösung. Wie Sie wissen, liefern Rechnungen mit Fließkommazahlen selten exakte, meist durch kleine Rundungsfehler geringfügig ungenaue Ergebnisse. Deshalb ist es keine gute Idee, `rad == 0.0` in der if-Bedingung zu schreiben. Ein Schwellwert, wann wir den Absolutwert der Zahl als so gut wie Null betrachten, muss unterschritten werden.
----
double rad = Math.pow(p / 2.0, 2) - q;
if (Math.abs(rad) < 1e-10) {
  printf("eine Lösung\n");
  printf("x = %.3f\n", - p / 2.0);
} else if (rad > 0) {
  printf("zwei Lösungen\n");
  printf("x1 = %.3f\n", - p / 2.0 + Math.sqrt(rad));
  printf("x2 = %.3f\n", - p / 2.0 - Math.sqrt(rad));
} else {
  printf("keine Lösung\n");
}
----
endif::solution[]

== Schleifen

=== Summe

Lege eine Variable `sum` an, initialisiere sie mit Null und schreibe ein Programmfragment, das die Summe der Zahlen von 1 bis 100 errechnet.

ifdef::solution[]
.Lösung
----
int sum = 0;
for(int i = 1; i <= 100; i++) sum += i;
----
endif::solution[]

=== Fizz
Schreiben Sie ein Programmfragment, das alle Zahlen von 1 bis 100 auf der Konsole ausgibt. Jede Zahl, die durch drei Teilbar ist soll dabei aber durch den Text "Fizz" ersetzt werden.

ifdef::solution[]
.Lösung
----
for(int i = 1; i <= 100; i++) {
  if(i % 3 == 0) {
    printf("fizz\n");
  } else {
    printf("%d\n", i);
  }
}
----
endif::solution[]

=== Fizzbuzz
Erweitern Sie Ihr Fizz-Programm so, dass nun auch jede durch 5 teilbare Zahl durch den Text "Buzz" ersetzt wird. Jede Zahl, die sowohl durch 3 als auch durch 5 teilbar ist, wird durch den Text "Fizzbuzz" ersetzt.

ifdef::solution[]
.Lösung
----
for(int i = 1; i <= 100; i++) {
  String s = "";
  if (i % 3 == 0) { s += "fizz"; }
  if (i % 5 == 0) { s += "buzz"; }
  if (s.length() > 0) {
    printf("%s\n", s);
  } else {
    printf("%d\n", i);
  }
}
----
endif::solution[]

=== 99 Bottles of Beer
Schreiben Sie ein Programm, das den Text des Liedes "99 bottles of beer" auf der Konsole ausgibt.

----
99 bottles of beer on the wall, 99 bottles of beer.
Take one down and pass it around, 98 bottles of beer on the wall.

98 bottles of beer on the wall, 98 bottles of beer.
Take one down and pass it around, 97 bottles of beer on the wall.
----
Und so weiter. Bis das Lied abschließt mit:
----
2 bottles of beer on the wall, 2 bottles of beer.
Take one down and pass it around, 1 bottle of beer on the wall.

1 bottle of beer on the wall, 1 bottle of beer.
Take one down and pass it around, no more bottles of beer on the wall.

No more bottles of beer on the wall, no more bottles of beer.
Go to the store and buy some more, 99 bottles of beer on the wall.
----

ifdef::solution[]
.Lösung
----
//Lösung 1: Minimale Redundanz
for(int i = 99; i >= 0; i--) {
  String postfix1 = i == 1 ? "" : "s";
  String postfix2 = i == 2 ? "" : "s";
  String number1 = i == 0 ? "no more" : Integer.toString(i);
  String number2 = i == 1 ? "no more" : Integer.toString(i-1);
  printf("%1$s bottle%3$s of beer on the wall, %2$s bottle%3$s of beer.\n",
         number1.substring(0, 1).toUpperCase() + number1.substring(1),
         number1,
         postfix1);
  if (i > 0) {
    printf("Take one down and pass it around, %s bottle%s of beer on the wall.\n",
           number2,
           postfix2);
  }
}
printf("Go to the store and buy some more, 99 bottles of beer on the wall.\n");

//Lösung 2: Beste Lesbarkeit bei großer Redundanz
for(int i = 99; i > 2; i--) {
  printf("%1$d bottles of beer on the wall, %1$d bottles of beer.\n", i);
  printf("Take one down and pass it around, %d bottles of beer on the wall.\n", i-1);
}
printf("2 bottles of beer on the wall, 2 bottles of beer.\n");
printf("Go to the store and buy some more, 1 bottle of beer on the wall.\n");
printf("1 bottle of beer on the wall, 1 bottle of beer.\n");
printf("Go to the store and buy some more, 99 bottles of beer on the wall.\n");
printf("No more bottles of beer on the wall, no more bottles of beer.\n");
printf("Go to the store and buy some more, 99 bottles of beer on the wall.\n");
----

Beide Lösungen haben Vor- und Nachteile. Lösung 1 verkompliziert den Code in der Schleife eigentlich unnötig für Ausnahmen, die nur in den drei letzten Durchläufen der Schleife relevant werden. Dafür taucht der Text der Strophen auch wirklich nur ein einziges mal im Code auf. Bei Lösung 2 könnte die Redundanz dazu führen, dass man einen Tippfehler im Strophentext an bis zu 4 Stellen korrigieren müsste.

Es ist erstaunlich schwierig eine elegante lesbare Lösung zu finden, die den Liedtext trotzdem völlig korrekt ausgibt. Vielleicht finden Sie ja eine schönere Variante als diese Musterlösungen?
endif::solution[]

== Methoden

////
=== Verdopplung
Erklären Sie das Verhalten der folgenden Methode, die das erste Element eines Arrays verdoppelt.
////

=== Meine erste Methode

Implementieren Sie die folgenden mathematischen Funktionen in Java:

* f(x) = x^2
* g(x) = x + 3
* h(x) = 2^x
* k(x,y) wobei gilt +
  k(x,y) = 2 * y, falls x >= 15 +
  k(x,y) = 4*y + 3, falls x < 15

ifdef::solution[]
.Lösung
----
double f(double x) { return Math.pow(x, 2); }
double g(double x) { return x + 3; }
double h(double x) { return Math.pow(2, x); }
double k(double x, double y) {
  if (x >= 15) {
    return 2 * y;
  } else {
    return 4 * y + 3;
  }
}
----
endif::solution[]

=== Zu viel Fun

Betrachten Sie die folgenden Methodendefinitionen.

  int fun(int x) {
    return x + 1;
  }

  double fun(double x) {
    return x + 2;
  }

  float fun(float x) {
    return x + 3;
  }

  double fun(double x, long y) {
    return x + 4;
  }

  boolean fun(int x, int y) {
    return x == y;
  }

Welche Methode wird bei den folgenden Aufrufen ausgeführt? Gibt es Aufrufe, die zu Fehlern führen?

* `fun(10)`
* `fun(10.0)`
* `fun(10f)`
* `fun(10, 10)`
* `fun(10, 10l)`
* `fun(10l, 10l)`
* `fun(10.5f, 10)`

ifdef::solution[]
.Lösung
Keiner der Aufrufe produziert einen Fehler. Java wählt immer die Methode aus, die "am besten" auf die Typen der Argumente passt. Beim Aufruf von `fun(10f)` wird z.B. die Methode ausgewählt, deren Parameter vom Typ `float` ist, da die `10f` diesen Typ vorgibt. Es wird also der spezifische Typ gewählt, selbst wenn prinzipell auch die Methode mit dem Parametertyp `double` in der Lage wäre, den `float` zu verarbeiten.
endif::solution[]

=== Überladung im Hirn

Welche der folgenden Methodenpaare dürfen zusammen definiert werden? Begründen Sie ihre Antwort.

. Paar
+
----
int fun() { ... }
double fun() { ... }
----

. Paar
+
----
int fun(String s) { ... }
double fun(boolean b) { ... }
----

. Paar
+
----
int fun(double d) { ... }
int fun(double d1, double d2) { ... }
----

. Paar
+
----
int fun(double d1) { ... }
int fun(double d2) { ... }
----

. Paar
+
----
int fun(double d) { ... }
int fun(double d, String s) { ... }
----

ifdef::solution[]
.Lösung
Die Methoden müssen unterscheidbar sein anhand der Anzahl und des Typs der Parameter. Das ist bei allen Methodenpaaren gegeben mit Ausnahme des ersten und vierten Paares.
endif::solution[]

=== Fehlersuche

Welche Fehler finden sich in den folgenden Methoden? Sehen Sie das Problem, bevor Sie den Fehler mithilfe der JShell suchen?

  int pow3(int number) {
    int erg = number * number * number;
  }

  int add(int x1, int x2) {
    int res = 0;
    return res;
    res = x1 + x2 ;
  }

  int mean(int x, int y) {
    return Math.abs(x - y) / 2.0;
  }

ifdef::solution[]
.Lösung
Bei `pow3` fehlt der Aufruf von `return`, bei `add` ist er eine Zeile zu früh gesetzt, sodass _toter Code_ entsteht, und bei `mean` stimmt der angegebene Rückgabetyp nicht mit dem Typ des Ausdrucks hinter dem `return` überein.
endif::solution[]

=== Größter gemeinsamer Teiler (ggT)

Schreiben Sie eine Methode `int ggt(int a, int b)`, die den größten gemeinsamen Teiler zweier Ganzzahlen berechnet.

Der zu verwendende https://en.wikipedia.org/wiki/Greatest_common_divisor#Using_Euclid.27s_algorithm[Algorithmus nach Euklid] ist in der englischen Wikipedia wie folgt beschrieben; dort heißt es nur "gcd" (_greatest common divisor_) statt ggT.

====
ggT(a,a) = a +
ggT(a,b) = ggT(a-b,b) für a>b +
ggT(a,b) = ggT(a,b-a) für b>a
====

. Arbeiten Sie mit einer `while`-Schleife, in der die Berechnung vollständig durchgeführt wird.
. Wenn Sie wissen, was Rekursion ist (der Selbstaufruf einer Methode): Implementieren Sie die Methode rekursiv.

ifdef::solution[]
.Erste Lösung mit while
----
int ggT(int a, int b) {
	while (a != b) {
		if (a > b) a = a - b;
		else b = b - a;
	}
	return a;
}
----

.Zweite, rekursive Lösung
----
int ggT(int a, int b) {
  if (a == b) return a;
  if (a > b) return ggT(a-b,b);
  else return ggT(a,b-a);
}
----
endif::solution[]

=== &#960; nach Gregory-Leibniz
Schreiben Sie eine Methode, die die Kreiszahl &#960; nach der unendlichen Reihe von Gregory-Leibniz berechnet (auch https://de.wikipedia.org/wiki/Leibniz-Reihe[Leibniz-Reihe] genannt), die wie folgt definiert ist:

&#960;/4 = 1 - 1/3 + 1/5 - 1/7 + 1/9 - ...

Ihre Methode soll ein Argument übernehmen mit dem man die Anzahl der Reihenglieder angeben kann, die für die Berechnung verwendet werden.

ifdef::solution[]
.Lösung
----
double piGL(int n) {
  double res = 0;
  for(int i = 0; i < n; i++) {
    double member = 1.0 / (1 + 2 * i);
    res += member * (i % 2 == 0 ? 1 : -1);
  }
  return res * 4;
}
----
endif::solution[]

=== &#960; nach Nilakantha
Schreiben Sie eine Methode, die &#960; nach der unendlichen Reihe von Nilakantha berechnet, die wie folgt definiert ist:

&#960; = 3 + 4/(2 * 3 * 4) - 4/(4 * 5 * 6) + 4/(6 * 7 * 8) - ...

Ihre Methode soll ein Argument übernehmen, mit dem man die Anzahl der Reihengileder angeben kann, die für die Berechnung verwendet werden.

ifdef::solution[]
.Lösung
----
double piN(int n) {
  double res = 3;
  for(int i = 1; i < n; i++) {
    double tmp = i * 2;
    double member = 4.0 / (tmp * (tmp+1) * (tmp+2));
    res += member * (i % 2 == 0 ? -1 : 1);
  }
  return res;
}
----
endif::solution[]

=== Range
Schreiben Sie eine Methode `range`, die zwei Ganzzahlen `s` und `e` als Argument übernimmt und ein Array zurückgibt, das alle ganzen Zahlen von `s` (inklusive) bis `e` (exklusive) enthält.

ifdef::solution[]
.Lösung
----
int[] range(int s, int e) {
  int[] res = new int[Math.max(e - s, 0)];
  for(int i = s; i < e; i++) {           // <1>
    res[i-s] = i;
  }
  return res;
}
----
<1> Natürlich kann man die Schleife auch wie gewohnt bei 0 anfangen und bis `e - s` laufen lassen. In diesem Fall ändert sich die Anweisung in der Schleife zu `res[i] = s + i`.
endif::solution[]

=== Range II
Erweitern Sie die `range` Methode so, dass sie jetzt ein drittes Argument `step` übernimmt, das die Schrittweite angibt. Der Aufruf von `range(0,8,3)` soll z.B. das Array `{0, 3, 6}` zurückgeben.

ifdef::solution[]
.Lösung
----
int[] range(int s, int e, int step) {
  int n = (e - s) / step
  int[] res = new int[Math.max(n, 0)];
  for(int i = 0; i < n; i++) {   // <1>
    res[i] = s + i * step;
  }
  return res;
}
----
<1> Die Variante, bei der die Schleife bei `s` startet, ist hier immer noch möglich, macht aber die Berechnung der Position im Array, in die geschrieben werden soll, unnötig kompliziert.
endif::solution[]

=== Beliebig viele Argumente

Schreiben Sie eine Methode `max`, die es erlaubt, das Maximum aus beliebig vielen Argumenten zu bestimmen.

----
jshell> max(7,5)
$1 ==> 7
jshell> max(3)
$2 ==> 3
jshell> max(3,7,10)
$3 ==> 10
----

ifdef::solution[]
.Lösung
----
double max(double... args) { <1>
  double max = Double.NEGATIVE_INFINITY;
  for(double d : args) {
    if (d > max) {
      max = d;
    }
  }
  return max;
}
----
<1> Der Typausdruck `double\...` bezeichnet einen sogenannten Varargs-Parameter. Innerhalb der Methode kann man die Variable `args` verwenden, als hätte sie den Typ `double[]`. Das eigentliche Array wird aber erst beim Aufruf der Methode aus den Argumenten erzeugt. Es darf pro Methode nur einen Varargs-Parameter geben, und dieser muss am Ende der Parameterliste stehen.
endif::solution[]

=== Das quadratische Rad neu erfinden

Implementieren Sie die folgenden Java-Methoden:

* Eine Methode `zero`, die keine Parameter entgegen nimmt und den Wert `0` als Integer zurückgibt.
* Eine Methode `succ` (_successor_, Nachfolger), die einen Integer `x` als Parameter übernimmt und den Wert `x + 1` zurückgibt.
* Eine Methode `add`, die zwei positive Integer `x` und `y` als Parameter übernimmt und diese addiert. Das Zeichen `+` darf ausschließlich im Kopf einer for-Schleife vorkommen. Sie müssen sich stattdessen mit der zuvor geschriebenen Methode `succ` behelfen.
* Eine Methode `mul`, die zwei positive Integer `x` und `y` als Parameter übernimmt und diese multipliziert. Das Zeichen `*` darf nicht in der Methodendefinition auftauchen. Verwenden Sie stattdessen die Methode `add`.
* Eine Methode `pow`, die zwei positive Integer `x` und `y` als Parameter übernimmt und das Ergebnis der Rechnung x^y (x "hoch" y) zurückgibt. Wieder dürfen Sie dazu nur ihre Methode `mul` verwenden.

Führen Sie nun die Rechnung 2^8 auf der JShell aus. Verwenden sie dazu ausschließlich die zuvor geschriebenen Methoden. Es sind auch keine Zahlliterale erlaubt.

=== Challenge: Deutsche Zahlen
Schaffen Sie es eine Methode zu schreiben, die eine natürliche Zahl von 0 bis 100 als deutsches Zahlwort zurückgibt?

    jshell> talk(0)
    $18 ==> "Null"

    jshell> talk(11)
    $19 ==> "Elf"

    jshell> talk(24)
    $20 ==> "Vierundzwanzig"
